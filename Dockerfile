FROM node:9-alpine AS builder
ENV NPM_CONFIG_LOGLEVEL warn
WORKDIR /home/node/app
COPY package.json package.json
RUN npm install
COPY . .
RUN npm run build --production

FROM node:9-alpine
ENV NODE_ENV=production
WORKDIR /home/node/app
COPY --from=builder /home/node/app/build ./build
RUN npm install -g serve

CMD serve -s build
