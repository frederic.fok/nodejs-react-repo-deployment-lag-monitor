# Repository Deployment Lag Monitor

[![pipeline status](https://gitlab.com/frederic.fok/nodejs-react-repo-deployment-lag-monitor/badges/master/pipeline.svg)](https://gitlab.com/frederic.fok/nodejs-react-repo-deployment-lag-monitor/commits/master)
[![coverage report](https://gitlab.com/frederic.fok/nodejs-react-repo-deployment-lag-monitor/badges/master/coverage.svg)](https://gitlab.com/frederic.fok/nodejs-react-repo-deployment-lag-monitor/commits/master)

 ```
 ________________________________________________________________________________
< GitLab Repository Deployment Lag Monitor in a Continuous Delivery environment>
 --------------------------------------------------------------------------------

All ants are mission-driven and carries their artefacts from one point to a target destination. 
Through their journeys, they overcome the challenges on their way...     
    \(")/       \(")/   \(")/       \(")/
    -( )-       -( )-   -( )-       -( )-
    /(_)\       /(_)\   /(_)\       /(_)\
  
The purpose of this React Application is to monitor a set of gitlab repositories to check indicators regarding their continuous delivery status. 

  
```

## Features

Feature-1. The initial feature exposed by this Monitoring App is to inform if any given repository have pending Merge Requests not yet deployed. 
- Benefits: quickly check if there are features that should be merged and deployed to a \
    target environment (eg. prod) more often
- How this is achieved: by checking the pipeline deployment status of the last commit on a given environment (eg. production) \
    and git comparing this commit id against the commit Id on the monitored branch (eg. master). \
    As such this gives an indication on whether there are outstanding Merge Requests not yet deployed into eg. production   

## How to

- Update the `src/config.js` file to add the gitlab repository to monitor 

- Set the `REACT_APP_GITLAB_API_KEY` environment variable to your gitlab API Key 

