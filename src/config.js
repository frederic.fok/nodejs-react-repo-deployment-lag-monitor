require('dotenv').config();

const env = process.env.NODE_ENV;

const development = {
    repos: [
        {
            name: 'py-project-variables-to-octopus',
            id: '7938615',
            env: 'staging',
            branch: 'master'
        }
    ],
    apiKey: process.env.REACT_APP_GITLAB_API_KEY
};

const production = {
    repos: [
        {
            name: 'py-project-variables-to-octopus',
            id: '7938615',
            env: 'staging',
            branch: 'master'
        }
    ],
    apiKey: process.env.REACT_APP_GITLAB_API_KEY
};

const scopedConfig = {
    development,
    production
};

export const config = scopedConfig[env]||development;