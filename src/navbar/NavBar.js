import React from 'react';

function NavBar() {
    return (
        <nav className="navbar navbar-dark bg-primary fixed-top">
            <div className="navbar-brand" to="/">
                Repo Monitor
            </div>
        </nav>
    );
}

export default NavBar;