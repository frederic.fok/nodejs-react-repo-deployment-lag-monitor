import React, { Component } from 'react';
import NavBar from "./navbar/NavBar";
import Repos from "./repos/Repos";

import { config } from "./config.js";

const {repos} = config;
const {apiKey} = config;

class App extends Component {
    render() {
        return (
            <div>
                <NavBar/>
                <Repos repos={repos} apikey={apiKey}/>
            </div>
        );
    }
}

export default App;