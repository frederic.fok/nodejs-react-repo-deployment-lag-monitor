import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import axios from 'axios';

class Repos extends Component {
    constructor(props) {
        super(props);

        this.repos = props.repos;
        this.apikey = props.apikey;
        this.state = {
            listRepoData: null,
        };
    }

    async componentDidMount() {

        this.forceUpdate.bind(this);
        this._isMounted = true;

        const listRepoData = await Promise.all(this.repos.map(async(repo) => {
            const lastDeployedCommit = await this.getLastDeployedCommit(repo.id, repo.env);
            const compares = lastDeployedCommit?await this.getCompares(repo.id, lastDeployedCommit.commitId, repo.branch):null;
            return this.updateRepoInfo(repo.name, compares);
        }))

        if (this._isMounted){
            this.setState({
                listRepoData,
            });
        }

    }

    componentWillUnmount() {
        this._isMounted = false
    }

    updateRepoInfo(repo, compares) {
        if (compares === null) {
            return {
                repo_name: repo,
                merges_ahead: 'unknown',
                oldest_merge: 'unknown'
            };
        }

        // a merge happened where a given commit has more than one parent
        const mergeCommits = compares.commits.filter(function (commit) {
            return commit.parent_ids.length > 1;
        });

        return {
            repo_name: repo,
            merges_ahead: mergeCommits.length,
            oldest_merge: mergeCommits.length ? mergeCommits[0].committed_date : null
        };
    }

    // return the last commit of the project projectId for the targeted environment (targetEnv)
    async getLastDeployedCommit(projectId, targetEnv) {
        let lastCommit = null;
        try {
            const deployments = (await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/deployments?order_by=created_at&sort=desc`, {headers: {'PRIVATE-TOKEN': `${this.apikey}`}})).data;
            const targetedEnvDeployments = deployments.filter(function (deployableItem) {
                return deployableItem.environment.name === targetEnv;
            });

            if (targetedEnvDeployments.length > 0) {
                lastCommit = {
                    date: targetedEnvDeployments[0].deployable.commit.committed_date,
                    commitId: targetedEnvDeployments[0].deployable.commit.id
                };
            }
        } catch (error){
            return lastCommit;
        }
        return lastCommit;
    }

    async getCompares(projectId, from, to) {
        try {
            return (await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/repository/compare?from=${from}&to=${to}`, {headers: {'PRIVATE-TOKEN': `${this.apikey}`}})).data;
        } catch (error){
            return null;
        }
    }

    render() {
        return (
            <div>
                {this.state.listRepoData === null && <p>Loading repo ...</p>}
                <BootstrapTable data={this.state.listRepoData} version='4'>
                    <TableHeaderColumn isKey dataField='repo_name'>Repositories</TableHeaderColumn>
                    <TableHeaderColumn dataField='merges_ahead'>Undeployed Merges</TableHeaderColumn>
                    <TableHeaderColumn dataField='oldest_merge'>Oldest Merge</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Repos;